" normal mappings
nnoremap <buffer> <leader>ca :OmniSharpGetCodeActions<cr>
nnoremap <buffer> <leader>cf :OmniSharpCodeFormat<cr>
nnoremap <buffer> <leader>dc :OmniSharpDocumentation<cr>
nnoremap <buffer> <leader>fi :call FindImplementations()<cr>
nnoremap <buffer> <leader>fm :call FindMembers()<cr>
nnoremap <buffer> <leader>fs :OmniSharpFindSymbol<cr>
nnoremap <buffer> <leader>ft :OmniSharpFindType<cr>
nnoremap <buffer> <leader>fu :call FindUsages()<cr>
nnoremap <buffer> <leader>fx :OmniSharpFixUsings<cr>
nnoremap <buffer> <leader>gd :OmniSharpGotoDefinition<cr>
nnoremap <buffer> <leader>mv :MoveItem<space>
nnoremap <buffer> <leader>ni :NewItem<space>
nnoremap <buffer> <leader>ri :update \| :OmniSharpRename<cr>
nnoremap <buffer> <leader>tl :OmniSharpTypeLookup<cr>
nnoremap <buffer> <leader>ga :call GotoAlternateFile()<cr>

nnoremap <buffer> <leader>af :AddField<space>
nnoremap <buffer> <leader>au :AddUsing<space>
nnoremap <buffer> <leader>rt :call RunTestsInFile()<cr>
nnoremap <buffer> <leader>td :call Epic_cs_testTeardown()<cr>
nnoremap <buffer> <leader>di :call Epic_cs_deleteItem(expand("%"))<cr>
nnoremap <buffer> <leader>tn :call Epic_cs_testNew()<cr>
nnoremap <buffer> <leader>ts :call Epic_cs_testSetup()<cr>

" visual mappings
vnoremap <buffer> <leader>ca :call OmniSharp#GetCodeActions('visual')<cr>
vnoremap <buffer> <leader>ev y:ExtractVariable <c-r>"<cr>

" insert mappings
inoremap <buffer> <c-n> <c-x><c-o>
inoremap <buffer> {{ <esc>o{<cr>}<esc>O

" global mappings based on file type
nnoremap <leader>rat :call RunAllTests()<cr>
nnoremap <leader>/ :FindInSolution<space>
