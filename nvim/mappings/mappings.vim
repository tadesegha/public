" ---------------
" global mappings
" ---------------
let mapleader = " "

nnoremap <leader>rn :set relativenumber!<cr>
nnoremap <leader>bd :silent! bufdo bd<cr>
nnoremap <leader>nt :e term://powershell<cr>
nnoremap <leader>t :buffer term<cr>
nnoremap ; :
nnoremap : ;
noremap <leader>y "*y
noremap <leader>p "*p
noremap <leader>P "*P
nnoremap <leader><leader> <c-^>
nnoremap <leader>w :update<cr>

" ---------------
" json mappings
" ---------------
vnoremap <leader>cf :'<,'>! python -m json.tool<cr>

" ---------------
" file mappings
" ---------------
nnoremap <leader>E :Files<cr>
nnoremap <leader>B :Buffers<cr>
nnoremap <c-s> :w<cr>

" ---------------
" window mappings
" ---------------
nnoremap <c-l> <c-w>l
nnoremap <c-h> <c-w>h
nnoremap <c-j> <c-w>j
nnoremap <c-k> <c-w>k

" -------------------
" configuration files
" -------------------
nnoremap <leader>init :edit ~/.common/public/nvim/init.vim<cr>
nnoremap <leader>ginit :edit ~/.common/public/nvim/ginit.vim<cr>

" -----------------
" terminal mappings
" -----------------
tnoremap <c-space> <c-\><c-n>
tnoremap <c-l> <c-\><c-n><c-w>l
tnoremap <c-h> <c-\><c-n><c-w>h
tnoremap <c-j> <c-\><c-n><c-w>j
tnoremap <c-k> <c-\><c-n><c-w>k
tnoremap <c-u> <esc>

" -----------------
" csharp mappings
" -----------------
execute "source " . g:nvimfiles . "/mappings/cs.vim"
