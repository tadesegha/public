function! Epic_global_getInput(message)
  call inputsave()
  let userInput = input(a:message . ": ")
  call inputrestore()

  return userInput
endfunction

function! IsEmpty(variable)
  return (a:variable == "")
endfunction

function! Epic_global_newGuid()
  let position = getpos('.')
  let guid = call('system', ['powershell.exe -noprofile -command "[Guid]::NewGuid().ToString()"'])
  put =guid

  normal "zd$
  delete
  call setpos('.', position)
  normal "zp
endfunction

function! Epic_global_insertIfTerminal()
  if (&buftype ==# 'terminal')
    silent! exe "normal a"
  endif
endfunction

function! Epic_global_isEmptyLine()
  let lineNum = line('.')
  let currLine = getline(lineNum)

  return (currLine =~ "^$")
endfunction
