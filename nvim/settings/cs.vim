" vim settings
setlocal shiftwidth=4
setlocal tabstop=4
setlocal softtabstop=4
setlocal omnifunc=OmniSharp#Complete
set errorformat=\ %#%f(%l\\\,%c):\ %m
set makeprg=msbuild\ /nologo\ /v:q\ /property:GenerateFullPaths=true\ /property:WarningLevel=0
