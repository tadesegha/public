$module = Get-Module -Name BurntToast

if ($module -eq $null) {
  Import-Module -Name BurntToast
}

New-BurntToastNotification -Text 'Drink some water', 'It is time to drink some water' -AppLogo c:\ta\public\images\water-drop.jpg
