param ($computerName = (read-host 'computer name'),
	$username = (read-host 'username'),
	$password = (read-host 'password'))

$props = @{ computerName = $computerName; username = $username; password = $password }
$content = new-object -typename PSCustomObject -property $props
$scriptsDirectory = "$root/local/scripts"

if (-not (test-path $scriptsDirectory)) {
  new-item -itemtype directory -path $scriptsDirectory -force
}

$file = "$scriptsDirectory/rdp.$password.ps1" 
set-content -path $file -value ('$computerName = "{0}"' -f $computerName)
add-content -path $file -value ('$username = "{0}"' -f $username)
add-content -path $file -value ('$password = "{0}"' -f $password)
add-content -path $file -value ('$credential = new.credential.ps1 -username $username -password $password')
add-content -path $file -value ('connect.to.remote.desktop.ps1 -computername $computerName -credential $credential')

store.password.ps1 -key $password
