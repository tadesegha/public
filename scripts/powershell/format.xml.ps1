param($text = (read-host 'xml'))

$xmlDocument = New-Object System.Xml.XmlDataDocument
$xmlDocument.LoadXml($text)

$stringWriter = New-Object System.Io.Stringwriter

$xmlWriter=New-Object System.Xml.XmlTextWriter($stringWriter)
$xmlWriter.Formatting = [System.Xml.Formatting]::Indented

$xmlDocument.WriteContentTo($xmlWriter)
$stringWriter.ToString()
