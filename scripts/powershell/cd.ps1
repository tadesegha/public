param ($path)

set-location $path

$item = get-item .
if ($item.GetFiles().Count + $item.GetDirectories().Count -lt 50) {
  get-childitem
}
