function NearestQuarterHour {
  $now = [DateTime]::Now
  $minutes = $now.Minute

  $minutesFromNextQuarter = 15 - ($minutes % 15)

  return $now.AddMinutes($minutesFromNextQuarter)
}

function CreateTrigger {
  $every15Minutes = New-TimeSpan -Minutes 15

  return New-ScheduledTaskTrigger -Once -At (NearestQuarterHour) -RepetitionInterval $every15Minutes 
}

$userName = [System.Security.Principal.WindowsIdentity]::GetCurrent().Name

$action = New-ScheduledTaskAction -Execute 'powershell' -argument "-windowstyle hidden -command ""$myscripts/powershell/water.notification.ps1"""
$trigger = CreateTrigger
$user = New-ScheduledTaskPrincipal $userName -runlevel Highest

$settings = New-ScheduledTaskSettingsSet
$settings.StopIfGoingOnBatteries = $false
$settings.DisallowStartIfOnBatteries = $false

$task = New-ScheduledTask -Action $action -Principal $user -Trigger $trigger -Settings $settings
Register-ScheduledTask "water.reminder" -InputObject $task
