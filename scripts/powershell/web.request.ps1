param ([parameter(mandatory = $true)] $baseUrl,
  $resourceUri = '/',
  $params = @{},
  $body = [string]::empty,
  $headers = @{},
  [switch] $unpackResponse = $true)

function Failure {
  Write-Host -ForegroundColor:Red "status: womp .. an error occurred."
  write-host -ForegroundColor:Red $_

  if ($_.Exception.Response) {
    $result = $_.Exception.Response.GetResponseStream()
    $reader = New-Object System.IO.StreamReader($result)
    $responseBody = $reader.ReadToEnd();
    Write-Host -BackgroundColor:Black -ForegroundColor:Red $responsebody
  }

  break
}

function GetQueryParams {
  $queryParams = [string]::Empty

  if ($params.Count -eq 0) { return $queryParams }

  foreach ($key in $params.Keys) {
    $value = $params[$key]
    $queryParams += "&$key=$value"
  }

  $queryParams = "?" + $queryParams.substring(1)
  return $queryParams
}

function Request {
  param ($uri)

  if ([string]::IsNullOrEmpty($body)) {
    $response = Invoke-WebRequest -Uri $uri -Method get -Headers $headers -ErrorAction:Stop
  } else {
    $response = Invoke-WebRequest -Uri $uri -Body $body -Method post -Headers $headers -ErrorAction:Stop
  }

  if ($unpackResponse) {
    return $response.Content
  }
  else {
    return $response
  }
}

$queryParams = GetQueryParams
$url = ("{0}{1}{2}" -f $baseUrl, $resourceUri, $queryParams)

try { Request $url }
catch { Failure }
