# environment settings
$env:TERM = ''
$env:HOME = "$env:HOMEDRIVE$env:HOMEPATH"

# variables
$myScripts = "$root/public/scripts"

# helper functions
if (test-path alias:\cd) {
  remove-item alias:\cd
}

& $myScripts/powershell/add.to.path "$myScripts/powershell"

# environment variables
add.to.path "$myScripts/cs"
add.to.path "$env:HOME/scripts"
add.to.path "c:/tools/neovim/neovim/bin"
add.to.path "C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\MSBuild\15.0\Bin"

# aliases
add.alias -Name e -Value nvim-qt
add.alias -Name grep -Value select-string
add.alias -Name nunit -Value nunit3-console
add.alias -name t -value "nunit-tests.ps1"
add.alias -name g -value git
add.alias -name b -value msbuild.ps1

Set-PSReadlineKeyHandler -Key Tab -Function Complete

# posh-git settings
# $GitPromptSettings.DefaultPromptSuffix = '`n$(''>'' * ($nestedPromptLevel + 1)) '
$GitPromptSettings.DefaultPromptPrefix = ([Environment]::NewLine)
$GitPromptSettings.DefaultPromptSuffix = "$([Environment]::NewLine)`$ "

# tls settings for invoke-webrequest
[Net.ServicePointManager]::SecurityProtocol = "tls12, tls11, tls"
