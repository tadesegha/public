param ($path)

if ($env:path.Contains($path)) {
  exit
}

$newPath = "$path;$env:path" -replace ';$', [string]::empty
$env:path = $newPath
