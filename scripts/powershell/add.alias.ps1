param ($name, $value)

if (test-path alias:\$name) {
  remove-item alias:\$name
}

new-alias -name $name -value $value -scope global
