param ($path)

if (-not (test-path $path)) {
  throw "can't find $path"
}

$assembly = get-item $path
[System.Reflection.AssemblyName]::GetAssemblyName($assembly.FullName).FullName
