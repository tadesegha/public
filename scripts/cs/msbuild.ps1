$msbuild = 'c:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\MSBuild\15.0\Bin\MSBuild.exe' 

if (-not (test-path $msbuild)) {
  throw "msbuild not found at location: $msbuild"
}

& $msbuild /v:minimal $args
