param ([array] $tests, $csproj)

if (-not $csproj) {
  $csprojs = (get-childitem *Test*.csproj -recurse)

  if (-not $csprojs -or $csprojs.Count -gt 1) {
    write-host 'either no test project found or too many test projects found'
    exit
  }

  $csproj = $csprojs[0]
}

$directory = get-item (split-path $csproj)

try {
  push-location $directory
  msbuild /verbosity:minimal

  if (command.failed) {
    exit
  }

  $testAssembly = (get-childitem -recurse -filter "$($directory.Name).dll")[0]

  set-location (split-path $testAssembly.FullName)

  if ($tests) {
    $testNames = $tests -join ','
    nunit $testAssembly.FullName --test $testNames
  } else {
    nunit $testAssembly.FullName
  }
}
finally {
  pop-location
}
