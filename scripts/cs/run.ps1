param ($csproj = (get-item '*.csproj'))

if (-not $csproj) {
  throw 'a project file is required'
}

msbuild /verbosity:minimal

if (command.failed) {
  exit
}

$exe = get-item ./bin/debug/**/*.exe
& $exe
