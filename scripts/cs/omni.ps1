param ([switch] $verbose)

function nextPort {
  param ($port = 2000)

  function matchesPort {
    param ($connection, $port)
    $connection.port -eq $port
  }

  $TCPProperties = [System.Net.NetworkInformation.IPGlobalProperties]::GetIPGlobalProperties()
  $connections = $TCPProperties.GetActiveTcpListeners()

  $matchingPorts = $connections |  where { matchesPort $_ $port }
  if ($matchingPorts.Count -gt 0) { return nextPort (++$port) }
  return $port
}

function startOmniServer {
  param ($sln, $port)

  $omnisharp = "c:/tools/omnisharp-roslyn/artifacts/scripts/omnisharp.http.cmd"
  set-location (get-location)
  msbuild /t:restore /t:build /verbosity:minimal

  if ($verbose) {
    & $omnisharp -p $port -s $sln -v
  } else {
    & $omnisharp -p $port -s $sln
  }
}

$sln = (get-childitem -recurse -filter *.sln)[0]
$sln = $sln.FullName -replace '\\', '/'

$port = nextPort
startOmniServer $sln $port
