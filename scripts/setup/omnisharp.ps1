git clone https://github.com/omnisharp/omnisharp-roslyn.git c:/tools/omnisharp-roslyn
push-location c:/tools/omnisharp-roslyn
try {
  ./build.ps1 -Target Quick
}
finally {
  pop-location
}

pip install neovim
