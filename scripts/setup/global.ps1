param ($root = (read-host root))

function LoadPowershellProfile {
	$psrc = "$root/public/scripts/powershell/psrc.ps1"

	if (-not (test-path $profile)) {
		new-item -itemtype file $profile -force
		add-content -path $profile -value ('$root = "{0}"' -f $root)
		add-content -path $profile -value (". $psrc")
	}

	. $psrc
}


$root = $root -replace '~', "$env:HOMEDRIVE$env:HOMEPATH"

Set-ExecutionPolicy Bypass -Scope Process -Force

# download chocolatey
# iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
choco feature enable -n allowGlobalConfirmation

# install tools
choco install git googlechrome openssh poshgit neovim python2 fzf

# download public folder in root location
& 'c:\program files\git\cmd\git.exe' clone https://bitbucket.org/tadesegha/public $root/public

LoadPowershellProfile

# autohotkey scheduled task
& $root/public/scripts/autohotkey/start.autohotkey.ps1
& $root/public/scripts/autohotkey/autohotkey.scheduled.task.ps1

# set up git config
$gitconfig = "$env:home/.gitconfig"
if (-not (test-path $gitconfig)) {
  new-item -itemtype symboliclink -path "$env:home" -name ".gitconfig" -target "$root/public/.gitconfig"
}

# set up init.vim
$nvimDirectory = "$env:home/appdata/local/nvim"
if (-not (test-path $nvimDirectory)) {
  new-item -itemtype directory -path $nvimDirectory
}

set-content -path $nvimDirectory/init.vim -value "source $root/public/nvim/init.vim"
set-content -path $nvimDirectory/ginit.vim -value "source $root/public/nvim/ginit.vim"

# set up minpac
git clone https://github.com/k-takata/minpac.git $env:home/appdata/local/nvim/pack/minpac/opt/minpac

# next steps
write-host @"
+ set up ssh key for bitbucket
+ update url for public folder to git@bitbucket:tadesegha/public in ($root/public/.git/config)
+ update nvim packages (:call minpac#update())

optional steps
+ install omnisharp ($root\public\scripts\setup\omnisharp.ps1)
+ install dropbox
"@
