# script variables
$script:autohotkey = "$myscripts\autohotkey\autohotkey.ahk"

# start autohotkey if it isn't already running
if (-not (get-process -processname autohotkey*)) {
  start "c:\program files\autohotkey\autohotkeyu64.exe" $script:autohotkey
}
