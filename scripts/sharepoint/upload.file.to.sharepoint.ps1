param ($siteUrl = (read-host 'Site url'),
    $siteRelativeFolderUrl = (read-host 'Site relative folder url'),
    $filePath = (read-host 'Filepath'))

$filepath = $filepath -replace '\\', '/'

$json = @"
  {
    "SiteRelativeFolderUrl" : "$siteRelativeFolderUrl",
    "Files" : [ "$filePath" ]
  }
"@

execute.deployment.step.ps1 -script uploadFiles -json $json
