param ( $siteUrl = ( read-host 'Site collection url' ),
  $siteRelativeFileUrl = ( read-host 'Site relative file url' ),
  $destination = ( read-host 'Destination path' ))

$json = @"
  {
    "siteRelativeFileUrls" : [ "$siteRelativeFileUrl" ],
    "destinationPaths" : [ "$destination" ]
	}
"@

execute.deployment.step.ps1 -script downloadFiles -json $json
