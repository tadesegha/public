param([array] $objects )

$ctxs = @{ }

foreach ( $object in $objects ) {
    $ctx = $object.context

    if ( -not ( $ctxs.contains( $ctx.web ))){
      $ctxs.add( $ctx.web, $ctx )
    }

    $ctx.Load( $object )
}

foreach ( $key in $ctxs.keys ){
  $ctxs[ $key ].ExecuteQuery( )
}
