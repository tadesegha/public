param ( $url = ( read-host 'site url' ))

$confirmation = read-host "confirm deletion of site ( $url )"
if ( -not ( $confirmation[0] -eq 'y' )){
  write-host 'site was not deleted'
  return
}

$json = @"
  {
    "siteUrls" : [ "$url" ]
  }
"@

execute.deployment.step.ps1 -script deleteSites -json $json
