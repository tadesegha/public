param ( $siteUrl = ( read-host 'site collection url' ),
  $siteRelativeFileUrl = ( read-host 'site relative file url' ))

$json = @"
  {
    "siteRelativeFileUrls" : [ "$siteRelativeFileUrl" ]
	}
"@

execute.deployment.step.ps1 -script deleteFiles -json $json
