param ( $url = ( read-host 'url' ),
  $clientIdKey = ( read-host 'client id key' ),
  $clientSecretKey = ( read-host 'client secret key' ))

try {

  $clientId = ( .get.secret 'dev.client.id' )
  $clientSecret = ( .get.secret 'dev.client.secret' )

  connect-pnpOnline -url $url -appId $clientId -appSecret $clientSecret

}
finally {

  $clientId = $null
  $clientSecret = $null
  
}
