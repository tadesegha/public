param ( $url = ( read-host 'url' ),
  $title = ( read-host 'title' ))

$json = @"
  {
    "Title" : "$title",
    "Url" : "$url"
  }
"@

execute.deployment.step.ps1 -script createSites -json $json
