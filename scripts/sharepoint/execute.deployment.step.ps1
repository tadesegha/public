param ( $script = ( read-host 'script' ),
  $json = ( read-host 'json' ))

$scriptsLocation = "$rootDirectory/common/projectTemplate/deploy/scripts"
$dllDirectory = "$rootDirectory/common/projectTemplate/deploy/dlls";

if ( -not ( test-path $dllDirectory/sharepoint/Microsoft.SharePoint.Client.dll )){
	throw "SharePoint client dlls are required at $dllDirectory/sharepoint"
}
Add-Type -LiteralPath $dllDirectory/sharepoint/Microsoft.SharePoint.Client.dll
Add-Type -LiteralPath $dllDirectory/sharepoint/Microsoft.SharePoint.Client.Runtime.dll
Add-Type -LiteralPath $dllDirectory/sharepoint/Microsoft.SharePoint.Client.Taxonomy.dll

. "$scriptsLocation/utility.ps1"

$json = ensure-configurationItem $json
& "$scriptsLocation/$script.ps1" -json $json
