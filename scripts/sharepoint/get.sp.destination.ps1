param( $file )

$filepath = $file.tostring( ).tolower( )
if ( $filepath.contains( 'sitepages' )){
   $index = $filepath.IndexOf( '\sitepages' )
} else { 
   $index = $filepath.IndexOf( '\siteassets' )
}

$dest = $file.Directory.ToString( ).substring( $index )
$dest = $dest.replace( '\', '/' )

return $dest
